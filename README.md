Moderation for Thunderbird
=======================

An extension intended to ease moderation tasks at the ENS.

Dependencies:
------------

The following Debian packages are enough: zip, ssh-askpass-gnome

How to install it:
------------------

Follow the instructions in the INSTALLATION file.

TODO list:
----------

* Detect incoming messages to moderate: done
* Display 2 buttons on them: done
* Add identity management (to authenticate the moderator) ;
* Moderate them using these buttons: done
* Detect mails telling that a mail has been moderated ;
* Update the status accordingly ;
* Add default response messages ;
* Conqueer the world.


